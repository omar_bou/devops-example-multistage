FROM maven:3-jdk-8-slim AS builder

WORKDIR /app

COPY pom.xml .

RUN mvn dependency:go-offline

COPY src/ /app/src/

RUN mvn package -DskipTests


#############


FROM openjdk:11-jdk-slim

EXPOSE 8080

WORKDIR /app

COPY  --from=builder /app/target/*.jar  /app/app.jar

CMD ["java", "-jar", "/app/app.jar"]
